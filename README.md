> Solution is not production-ready and only for http    
> there is no load balancer.  

> In my opinion, each connection in master-cluster should be    
> sended to one of (should be load balancing) child-process   
> (slave-cluster) for further connection handling.  
> But I could not implement this approach. 

### Installation
Install the dependencies and start test server.
```sh
$ npm install
$ npm run dev
```

### Todos
[ ] Implement approach with load balancing.   
[ ] Implement sending socket of connection to slave-cluster, instead of sending instance of server. 

