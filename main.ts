
import http from 'http'
import Service from './solution'
import { IOptions } from './ioptions'

const requestListener = (req:any, res:any) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' })
  res.end('PONG')
 
  if(process.argv[2]){
    console.log(process.argv[2], process.argv[3])
  }else{
    console.log('parent')
  }

}




let options:IOptions = {
  isClusterMode: true,
  transport :{
    name: 'http',
    server: http.createServer(requestListener),
    port: 3000
  },
  filename: __filename
}

let s = new Service(options)
s.start()

