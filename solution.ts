import { fork } from 'child_process'
import { IService } from './iservice'
import { IOptions } from './ioptions';

export default class Service implements IService {

    options: IOptions
    isMaster:boolean
    constructor(options: IOptions){
        this.isMaster = process.argv[2] !== 'child'
        this.options = options
    }

    async start() {
        if( this.options.isClusterMode ) {
            if( this.isMaster ) {
                await this.startCluster();
                await this.startTransport();
            }else{
                await this.startWorker();
            }
        }
        else {
            await this.startTransport();
        }
    }

    async startTransport() {
        this.options.transport.server.listen(this.options.transport.port, () => {
                console.log(`server is listening on ${this.options.transport.port}`)
            });
    }

    async startCluster() {
        let numCores = require('os').cpus().length;
        let workers:any[] = []
        for (let i = 0; i < numCores; i++) {
            workers[i] = fork(this.options.filename, ['child', `${i}`]);
        }

        let server = this.options.transport.server
        server.on('listening', async () => {
             Promise.all(workers.map(async (worker) => {
                await worker.send('server', server)
            }))
        })

    }

    async startWorker() {
        let server = this.options.transport.server
        process.on('message', (msg, httpServer) => {
        if (msg === "server") {
            httpServer.on('connection', async (socket: any) => {
                await server.emit('connection', socket)})
            }
         })
    }

}
