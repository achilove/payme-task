
interface ITransport {
    name: string;
    server: any;
    port: number
}


export interface IOptions {
    isClusterMode : boolean;
    transport: ITransport
    filename: string
}