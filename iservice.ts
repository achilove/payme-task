import {IOptions} from './ioptions'

export interface IService {
    options: IOptions
    isMaster: boolean

    start():void;
    startTransport():void;
    startCluster():void;
    startWorker():void

}